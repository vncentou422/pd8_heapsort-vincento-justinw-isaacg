// Isaac Gluck
// APCS2 pd 8
// HW42
// 2014-05-22

import java.util.*;
import java.io.*;

/*
* HEAPSORT ALGORITHM
* The algorithm for heapsort works as follows:
*  1. heapify the array to start off with a heapified array 
*  2. for one full pass of the array
*    a) remove the root(the largest value) and hold on to it
*    b) swap it with the smallest non-sorted value
*    c) heapify up until the sorted region
*    d) add the old root to the sorted region and begin anew
* 3. Done!
*/

public class HeapSort {

	public int[] sort(int[] data) {
		data = heapify(data, data.length); // to start sorting to a preliminary heapify

		int sortedPartition = data.length - 1; // to begin, the sorted region will be at the end
		int toSort; // will temporarily hold the root to put in the sorted Partition
		while (sortedPartition > 0) { // while the entire array is not sorted
			toSort = data[0]; // remember the root
			swap(data, 0, sortedPartition); // replace the root with the smallest value
			heapify(data, sortedPartition); // re heapify excluding the sorted region
			data[sortedPartition] = toSort; // add the root to the sorted region
			sortedPartition--; // increase the sorted region
		}

		return data; // we out
	}

	// HELPERS
	public int[] heapify (int[] data, int endIndex) { // make the array follow max heap properties
		for (int i=1; i<endIndex; i++) {
			int index = i;
			int parIndex = getParent(index);
			// if a value is not in the right location, swap it there
			while (parIndex >= 0 && data[index] > data[parIndex]) {
				swap(data, index, parIndex);
				index = parIndex;
				parIndex = getParent(parIndex);
			}
		}
		return data;
	}

	public int getParent(int pos) { // find the parent index in the array
		return (pos - 1) / 2;
	}

	public void swap(int[] data, int index1, int index2) { // swap to values in the array
    	int temp = data[index1];
    	data[index1] = data[index2];
    	data[index2] = temp;
    }

    public static void main(String[] args) {
    	HeapSort hs = new HeapSort();

    	int[] test = {4,3,78,6,9,2};
    	for (int i=0;i<test.length;i++) {
    		System.out.print(test[i] + " ");
    	}
    	System.out.println();


    	test = hs.sort(test);

    	for (int i=0;i<test.length;i++) {
    		System.out.print(test[i] + " ");
    	}
    	System.out.println();
    }
}