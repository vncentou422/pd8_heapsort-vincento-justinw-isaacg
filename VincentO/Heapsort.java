//Vincent Ou
//APCS pd 8
//HW42
//2014-05-21

public class Heapsort{

    public int[] sort( int[] data ){
	int[] dataheap = heapify(data, data.length);
	int counter = data.length - 1;
	int x = 0;
	while(counter > 0){
	    x = data[0];
	    swap(data, 0, counter);
	    heapify(data,counter);
	    data[counter] = x;
	    counter--;
	    
	}
	return data;
    }
    public int[] heapify(int[] data, int index){
	for (int x = 0; x < index; x++){
	    int parentIndex = getParent(x);
	    while( parentIndex >= 0 && data[x] > data[parentIndex]){
		swap(data, x, parentIndex);
		x = parentIndex;
		parentIndex = getParent(x);
	    }
	}
	return data;
    }
    public void swap(int[] data, int index1, int index2){
	int tmp = data[index1];
	data[index1] = data[index2];
	data[index2] = tmp;
    }
    public int getParent(int index){
	return (index - 1)/2;
    }
    public String toString(int[] data){
	String retStr = "";
	for (int x = 0 ; x < data.length; x++){
	    retStr += data[x] + ",";
	}
	return retStr.substring(0,retStr.length() -1);
    }
    public static void main(String[] args){
	Heapsort y = new Heapsort();
	int[] x = {12,117,14,1,5,7,42,314};
	System.out.println(y.toString(x));
	System.out.println(y.toString(y.sort(x)));
	
    }
}
	
    
